const data = require('../../data.json');

module.exports = (req, res) => {
  const blogs = data.blogs;
  res.locals.blogs = blogs;
  res.render('blogs');
};