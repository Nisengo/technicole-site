const findObject = require('../../utils/findObject');
const blog = require('express').Router();
const all = require('./all');
const single = require('./single');

blog.get('/', all);

blog.param('blogId', findObject('blog'));

blog.get('/:blogId', single);

module.exports = blog;