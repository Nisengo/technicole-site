module.exports = (req, res) => {
  const blog = req.blog;
  res.locals.blog = blog;
  res.render('single-blog.pug');
};