const routes = require('express').Router();
const blog = require('./blog');

routes.use('/blog', blog);

routes.get('/', function (req, res, next) {
  res.render('home');
})

routes.get('/about', function (req, res, next) {
  res.render('about');
})

routes.get('/blog', function (req, res, next) {
  res.render('blogs');
})

module.exports = routes;