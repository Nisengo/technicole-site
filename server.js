var express = require('express'),
  logger = require('morgan'),
  request = require('request'),
  app = express(),
  path = require('path')
  routes = require('./source/routes');

app.use(logger('dev'));
app.use(express.static(__dirname + '/static'));
app.set('view engine', 'pug');
app.set("views", path.join(__dirname, "source/templates"));

app.use('/', routes);

app.listen(process.env.PORT || 3000, function () {
  console.log('Listening on http://localhost:' + (process.env.PORT || 3000));
})
